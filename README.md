# blog

![](https://img.shields.io/badge/written%20in-PHP-blue)

A static blog generator.

Only used internally, deprecated by updates to `codesite`.

Supports RSS, bbcode, and internal references.


## Download

- [⬇️ blog-0.7z](dist-archive/blog-0.7z) *(3.34 KiB)*
